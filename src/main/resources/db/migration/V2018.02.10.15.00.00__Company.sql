CREATE TABLE public.company
(
    company_id SERIAL PRIMARY KEY,
    company_in VARCHAR(100) NOT NULL,
    name VARCHAR(255) NOT NULL,
    user_updated_at TIMESTAMP NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE UNIQUE INDEX company_in_idx ON company (company_in);

COMMENT ON TABLE public.company IS 'It''s important to have not important comments.';

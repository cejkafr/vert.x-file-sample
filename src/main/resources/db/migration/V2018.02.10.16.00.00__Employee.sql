CREATE TABLE public.employee
(
    employee_id SERIAL PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    company_id BIGINT NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    user_updated_at TIMESTAMP NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT employeee_company_company_id_fk FOREIGN KEY (company_id) REFERENCES company (company_id)
);

CREATE UNIQUE INDEX email_idx ON employee (email);

COMMENT ON TABLE public.employee IS 'It''s important to have not important comments.';

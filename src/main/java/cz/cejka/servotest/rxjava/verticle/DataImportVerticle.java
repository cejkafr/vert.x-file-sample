package cz.cejka.servotest.rxjava.verticle;

import cz.cejka.servotest.persistence.MigrationUtils;
import cz.cejka.servotest.rxjava.datapersister.DataPersister;
import cz.cejka.servotest.rxjava.dataprocessor.DataProcessor;
import cz.cejka.servotest.rxjava.datareader.DataReaderFactory;
import cz.cejka.servotest.rxjava.persistence.repository.CompanyRepository;
import cz.cejka.servotest.rxjava.persistence.repository.SimpleCompanyRepository;
import cz.cejka.servotest.rxjava.persistence.repository.EmployeeRepository;
import cz.cejka.servotest.rxjava.persistence.repository.SimpleEmployeeRepository;
import io.vertx.core.Future;
import io.vertx.core.file.CopyOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.ext.jdbc.JDBCClient;
import io.vertx.rxjava.ext.sql.SQLClient;
import rx.Observable;
import rx.Single;

import java.io.File;
import java.time.Instant;
import java.util.stream.Collectors;

/**
 * Checks input directory for readable data files and imports their content to database.
 *
 * @author fcejka
 */
public class DataImportVerticle extends AbstractVerticle
{
	private final static Logger LOG = LoggerFactory.getLogger(DataImportVerticle.class);

	public final static int DEF_SCAN_TIMEOUT = 10000;
	public final static String DEF_INPUT_DIR = "input";
	public final static String DEF_OUTPUT_DIR = "output";

	// Configuration values, only few of them so I like to have them like this.
	private int dataScanTimeout;
	private String dataInputDir;
	private String dataCompletedDir;

	// Libs are created on start.
	private DataReaderFactory dataReaderFactory;

	private SQLClient sqlClient;
	private CompanyRepository companyRepository;
	private EmployeeRepository employeeRepository;

	/**
	 * This is called automatically by Vert.x handler at the verticle deployment.
	 *
	 * @param startFuture
	 * @throws Exception
	 */
	@Override
	public void start(Future<Void> startFuture) throws Exception
	{
		// Load config.
		JsonObject datasourceJson = config().getJsonObject("datasource", new JsonObject());
		JsonObject directoriesJson = config().getJsonObject("directories", new JsonObject());

		dataScanTimeout = config().getInteger("scanTimeout", DEF_SCAN_TIMEOUT);
		dataInputDir = directoriesJson.getString("input", DEF_INPUT_DIR);
		dataCompletedDir = directoriesJson.getString("completed", DEF_OUTPUT_DIR);

		LOG.info("Starting data import verticle (dataScanEach: {}ms, inputDir: {}, completedDir: {})",
				dataScanTimeout, dataInputDir, dataCompletedDir);

		// Create instances for runtime libs.
		dataReaderFactory = new DataReaderFactory(vertx, config().getJsonObject("datareader", new JsonObject()));
		sqlClient = JDBCClient.createShared(vertx, datasourceJson);
		companyRepository = new SimpleCompanyRepository();
		employeeRepository = new SimpleEmployeeRepository();

		// Execute validations & migrations.
		vertx.<Void>rxExecuteBlocking(event ->
		{
			MigrationUtils.migrate(datasourceJson);
			event.complete();
		})

				// Set up initial timer for first import process.
				.doOnSuccess(this::setUpImportProcessTimer)

				// Simple error handler
				.doOnError(LOG::error)

				// Subscribe to stream and notify Vert.x platform about result.
				.subscribe(startFuture::complete, startFuture::fail);
	}

	/**
	 * This is called automatically by Vert.x platform when its shutting down or verticle is undeployed.
	 *
	 * @param stopFuture
	 * @throws Exception
	 */
	@Override
	public void stop(Future<Void> stopFuture) throws Exception
	{
		// TODO: probably should not quit during import // possible with transaction

		stopFuture.complete();
	}


	/**
	 * Sets new timer for import process.
	 *
	 * @param aVoid
	 */
	private void setUpImportProcessTimer(Void aVoid)
	{
		vertx.timerStream(dataScanTimeout)
				.toObservable()
				.subscribe(this::runImportProcess);
	}

	/**
	 * Executes import process:
	 * - check for files
	 * - import files
	 *
	 * @param aLong
	 */
	private void runImportProcess(long aLong)
	{
		LOG.debug("Running process.");

		// Read files
		findNextDataFile()

				// For each file ...
				.flatMapSingle(path ->
				{
					final JsonObject stats = new JsonObject()
							.put("path", path)
							.put("startedAt", Instant.now());

					LOG.info("Processing file {}.", path);

					// Prepare reader
					return dataReaderFactory.openFile(path)

							// Prepare data processor - reads all data, creates CompanyDataObject and EmployeeDTO, counts duplicated entries
							.flatMap(DataProcessor::processSimple)
							.doOnSuccess(result -> stats.mergeIn(result.stats))

							// Persist processed data
							.flatMap(result -> DataPersister.persistSimple(sqlClient, companyRepository, employeeRepository, result))
							.doOnSuccess(result -> stats.mergeIn(result.stats))


							// Move file to completed directory - replacing any existing files
							// TODO: maybe add suffix, can be problem if user uploads always same file
							.flatMap(result -> vertx.fileSystem()
									.rxMove(path, getCompletedDirPath(path), new CopyOptions().setReplaceExisting(true)))

							.map(result -> stats.put("finishedAt", Instant.now()));
				})

				.doOnNext(this::printStats)

				// Set new up timer.
				.doOnTerminate(() -> setUpImportProcessTimer(null))

				// Error handler
				.doOnError(LOG::error)

				// Observable must have subscriber.
				.subscribe();
	}

	/**
	 * Reads input dir for files and validates if they're processable.
	 * TODO: FILE ORDER IS LEFT TO VERT.X IMPLEMENTATION, POTENTIALLY DANGEROUS.
	 *
	 * @return
	 */
	private Observable<String> findNextDataFile()
	{
		return vertx.fileSystem()
				.rxReadDir(dataInputDir)
				.doOnSuccess(strings -> LOG.info("Reading {} input files.", strings.size()))
				.map(strings -> strings.stream()
						// TODO: do something with unreadable files.
						.filter(dataReaderFactory::isFileReadable)
						.collect(Collectors.toList()))
				.doOnSuccess(strings -> LOG.info("Recognized {} readable files.", strings.size()))

				// Return just one file at the time
				.flatMapObservable(strings ->
				{
					if (strings.isEmpty()) {
						return Observable.empty();
					} else {
						return Observable.just(strings.iterator().next());
					}
				});
	}

	private String getCompletedDirPath(String path)
	{
		File file = new File(path);
		return dataCompletedDir + File.separator + file.getName();
	}

	private void printStats(JsonObject stats)
	{
		Instant startedAt = stats.getInstant("startedAt");
		Instant finishedAt = stats.getInstant("finishedAt");
		double time = (finishedAt.toEpochMilli() - startedAt.toEpochMilli()) / 1000.0;

		StringBuilder sb = new StringBuilder("\n\n");
		sb.append("File: ").append(stats.getString("path")).append("\n");
		sb.append("Started at: ").append(startedAt).append("\n");
		sb.append("\n");

		sb.append("Processed ").append(stats.getInteger("totalEntryCount")).append(" entries total, found:\n");
		sb.append("\t").append(stats.getInteger("companyCount")).append(" companies; ").append(stats.getInteger("duplicateCompanyCount")).append(" duplicates\n");
		sb.append("\t").append(stats.getInteger("employeeCount")).append(" employees; ").append(stats.getInteger("duplicateEmployeeCount")).append(" duplicates\n");
		sb.append("\n");
		sb.append("Persistence statistics:\n");
		sb.append("\tcompanies -  ").append(stats.getInteger("companyInsertCount")).append("  insertions; ").append(stats.getInteger("companyUpdateCount")).append(" updates; ").append(stats.getInteger("companyDeleteCount")).append(" deletions\n");
		sb.append("\temployees -  ").append(stats.getInteger("employeeInsertCount")).append("  insertions; ").append(stats.getInteger("employeeUpdateCount")).append(" updates; ").append(stats.getInteger("employeeDeleteCount")).append(" deletions\n");
		sb.append("\n");

		sb.append("Finished at: ").append(finishedAt).append(" (").append(time).append("s)").append("\n");

		LOG.info(sb.toString());
		//System.out.println(sb.toString());
	}
}

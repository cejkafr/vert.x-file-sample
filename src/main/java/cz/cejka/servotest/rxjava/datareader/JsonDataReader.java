package cz.cejka.servotest.rxjava.datareader;

import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.core.parsetools.JsonParser;
import rx.Observable;

import static io.vertx.core.parsetools.JsonEventType.VALUE;

/**
 * Simple class to create abstraction for file format reading.
 * TODO: could be static?
 *
 * @author fcejka
 */
public class JsonDataReader implements DataReader
{
	private final JsonParser parser;

	public JsonDataReader(Observable<Buffer> stream)
	{
		parser = JsonParser.newParser(stream);
		parser.objectValueMode();
	}

	@Override
	public Observable<DataEntry> toObservable()
	{
		return parser.toObservable()
				.filter(jsonEvent -> jsonEvent.type() == VALUE)
				.map(jsonEvent -> jsonEvent.mapTo(DataEntry.class)); // TODO: generate static mapper for data entry
	}
}

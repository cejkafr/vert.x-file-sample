package cz.cejka.servotest.rxjava.datareader;

public class DataEntry
{
	private String companyIn;
	private String companyName;
	private String employeeEmail;
	private String employeeFirstname;
	private String employeeSurname;
	private String lastModifiedAt;

	public DataEntry()
	{
	}

	public String getCompanyIn()
	{
		return companyIn;
	}

	public String getCompanyName()
	{
		return companyName;
	}

	public String getEmployeeEmail()
	{
		return employeeEmail;
	}

	public String getEmployeeFirstname()
	{
		return employeeFirstname;
	}

	public String getEmployeeSurname()
	{
		return employeeSurname;
	}

	public String getLastModifiedAt()
	{
		return lastModifiedAt;
	}
}

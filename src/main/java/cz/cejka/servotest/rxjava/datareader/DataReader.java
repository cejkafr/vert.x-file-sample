package cz.cejka.servotest.rxjava.datareader;

import rx.Observable;

/**
 * Just to provide some abstraction to different formats.
 * TODO: probably could make it static to save some memory
 *
 * @author fcejka
 */
public interface DataReader
{
	Observable<DataEntry> toObservable();
}

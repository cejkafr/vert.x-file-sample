package cz.cejka.servotest.rxjava.datareader;

import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.buffer.Buffer;
import rx.Observable;
import rx.Single;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Provides us with ability to read different file formats defined in configuration.
 *
 * @author fcejka
 */
public class DataReaderFactory
{
	private final static Logger LOG = LoggerFactory.getLogger(DataReaderFactory.class);

	private final Vertx vertx;
	private final Map<String, Class> formatReaderMapping;

	public DataReaderFactory(Vertx vertx, JsonObject config) throws DataReaderException
	{
		this.vertx = vertx;

		if (config == null) {
			throw new DataReaderException("DataReader configuration error - configuration is missing.");
		}

		JsonArray formats = config.getJsonArray("formats", new JsonArray());

		this.formatReaderMapping = formats.stream()

				// Create list of JsonObjects
				.filter(JsonObject.class::isInstance) 	// TODO: print message that entry was ignored
				.map(JsonObject.class::cast)
				.collect(Collectors.toList())
				.stream()

				// Create map from valid objects
				.filter(this::validateJsonObject)		// TODO: print message that entry was ignored
				// TODO: possible NPE when previous filtering fails.
				.collect(Collectors.toMap(
						jo -> jo.getString("extension").toLowerCase(),
						jo -> stringToClass(jo.getString("className")))
				);

		if (this.formatReaderMapping.isEmpty())
		{
			throw new DataReaderException("DataReader configuration error - no formats are defined.");
		} else {
			LOG.info("DataReader successfully started, configured {} readers.", formatReaderMapping.size());
		}

	}

	/**
	 * Checks if filename is readable by defined reader.
	 * Warning: Validates only extension, not it's content!
	 *
	 * @param path
	 * @return
	 */
	public boolean isFileReadable(String path)
	{
		return formatReaderMapping.containsKey(getFileExt(path));
	}


	public Single<DataReader> openFile(String path) throws DataReaderException
	{
		Class readerClass = getReaderClass(path);

		final OpenOptions opts = new OpenOptions()
				.setWrite(false)
				.setRead(true);

		return vertx.fileSystem().rxOpen(path, opts)
				.map(asyncFile -> asyncFile.toObservable())
				.map(bufferObservable -> createReaderInstance(readerClass, bufferObservable));
	}

	public Observable<DataEntry> readFile(String path) throws DataReaderException
	{
		return openFile(path).flatMapObservable(DataReader::toObservable);
	}

	/**
	 * Validates format json object content.
	 * TODO: Add validation for className so we're sure class really exists.
	 * TODO: Add check if class is extending DataReader!
	 *
	 * @param jo
	 * @return
	 */
	private boolean validateJsonObject(JsonObject jo)
	{
		return jo.containsKey("extension") && jo.containsKey("className");
	}

	/**
	 * Create class from string, returns null if class not found.
	 *
	 * @param className
	 * @return
	 */
	private Class stringToClass(String className)
	{
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException ex) {
			LOG.error("Failed to initialize DataReader {}, class not found.", className);
			return null;
		}
	}

	/**
	 * Simple function to quickly get file extension.
	 * TODO: filename may not have extension, or it may end with dot, then we get out of range.
	 *
	 * @param path
	 * @return
	 */
	private String getFileExt(String path)
	{
		return path.substring(path.lastIndexOf(".") + 1).toLowerCase();
	}

	private Class getReaderClass(String path) throws DataReaderException
	{
		if (!isFileReadable(path)) {
			throw new DataReaderException("File " + path + " is not readable by any configured DataReader.");
		}

		return formatReaderMapping.get(getFileExt(path));
	}

	private DataReader createReaderInstance(Class className, Observable<Buffer> bufferObservable) throws DataReaderException
	{
		if (className == null) {
			throw new DataReaderException("Class name is null, cannot create instance of null.");
		}

		try {
			Constructor constructor = className.getConstructor(bufferObservable.getClass());
			return (DataReader) constructor.newInstance(bufferObservable);
		} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
			// TODO: Lets just assume, this didn't happen.
			throw new DataReaderException("Failed to create instance of DataReader.", ex);
		}
	}
}

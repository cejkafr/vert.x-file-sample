package cz.cejka.servotest.rxjava.datapersister;

import io.vertx.core.json.JsonObject;

public class DataPersisterResult
{
	public final JsonObject stats;

	public DataPersisterResult(JsonObject stats)
	{
		this.stats = stats;
	}
}

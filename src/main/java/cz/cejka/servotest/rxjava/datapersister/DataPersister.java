package cz.cejka.servotest.rxjava.datapersister;

import cz.cejka.servotest.rxjava.dataprocessor.DataProcessorResult;
import cz.cejka.servotest.rxjava.persistence.repository.CompanyRepository;
import cz.cejka.servotest.rxjava.persistence.repository.EmployeeRepository;
import io.vertx.rxjava.ext.sql.SQLClient;
import rx.Single;

public interface DataPersister
{
	static Single<DataPersisterResult> persistSimple(SQLClient sqlClient,
													 CompanyRepository companyRepository,
													 EmployeeRepository employeeRepository,
													 DataProcessorResult result)
	{

		return new SimpleDataPersister(sqlClient, companyRepository, employeeRepository).persist(result);
	}

	Single<DataPersisterResult> persist(DataProcessorResult result);
}

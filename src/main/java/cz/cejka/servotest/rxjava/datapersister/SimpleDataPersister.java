package cz.cejka.servotest.rxjava.datapersister;

import cz.cejka.servotest.rxjava.dataprocessor.CompanyDataObject;
import cz.cejka.servotest.rxjava.dataprocessor.DataProcessorResult;
import cz.cejka.servotest.rxjava.dataprocessor.EmployeeDataObject;
import cz.cejka.servotest.rxjava.persistence.repository.CompanyEntity;
import cz.cejka.servotest.rxjava.persistence.repository.CompanyRepository;
import cz.cejka.servotest.rxjava.persistence.repository.EmployeeEntity;
import cz.cejka.servotest.rxjava.persistence.repository.EmployeeRepository;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.ext.sql.SQLClient;
import io.vertx.rxjava.ext.sql.SQLConnection;
import rx.Observable;
import rx.Single;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author fcejka
 */
public class SimpleDataPersister implements DataPersister
{
	private class Stats
	{
		public int companyInsertCount = 0;
		public int companyUpdateCount = 0;
		public int companyDeleteCount = 0;
		public int employeeInsertCount = 0;
		public int employeeUpdateCount = 0;
		public int employeeDeleteCount = 0;

		public JsonObject toJson()
		{
			return new JsonObject()
					.put("companyInsertCount", companyInsertCount)
					.put("companyUpdateCount", companyUpdateCount)
					.put("companyDeleteCount", companyDeleteCount)
					.put("employeeInsertCount", employeeInsertCount)
					.put("employeeUpdateCount", employeeUpdateCount)
					.put("employeeDeleteCount", employeeDeleteCount);
		}
	}

	private final static Logger LOG = LoggerFactory.getLogger(SimpleDataPersister.class);

	private final Instant startInstant;
	private final SQLClient sqlClient;
	private final CompanyRepository companyRepository;
	private final EmployeeRepository employeeRepository;

	private Map<String, Long> companyInIdMap;
	private Stats stats;

	public SimpleDataPersister(SQLClient sqlClient, CompanyRepository companyRepository, EmployeeRepository employeeRepository)
	{
		startInstant = Instant.now();
		this.sqlClient = sqlClient;
		this.companyRepository = companyRepository;
		this.employeeRepository = employeeRepository;
	}

	@Override
	public Single<DataPersisterResult> persist(DataProcessorResult result)
	{
		companyInIdMap = new HashMap<>();
		stats = new Stats();

		// Get SQL Connection from the pool
		return sqlClient.rxGetConnection()
				.flatMap(conn -> {

					// Disable auto commit to enable transactions
					return conn.rxSetAutoCommit(false)

							// Persist companies
							.flatMap(aVoid -> persistCompanies(conn, result.companyList.values()))

							// Persist employees
							.flatMap(aInteger -> persistEmployees(conn, result.employeeList.values()))

							// Delete data not present in current data set
							//.flatMap(aInteger -> clearRemains(conn))

							// Commit transaction
							.flatMap(aVoid -> conn.rxCommit())

							// On error rollback
							.doOnError(throwable ->
							{
								LOG.error("Error occurred during persistor run.", throwable);
								conn.rxRollback().subscribe();
							})

							// Return connection to the pool
							.doAfterTerminate(conn::close);
				})

				// Create result
				.map(aVoid -> new DataPersisterResult(stats.toJson()));

				// Debug print result
				//.doOnSuccess(dataPersisterResult -> LOG.info(dataPersisterResult.stats));
	}

	private Single<Integer> persistCompanies(SQLConnection conn, Collection<CompanyDataObject> values)
	{
		return fetchCompanyUpdatedMap(conn)

				// Execute in context of previous stream
				.flatMap(updatedMap -> Observable.from(values)

						// Filter out companies that doesn't need to be updated
						.filter(company -> !updatedMap.containsKey(company.getIn()) ||
								updatedMap.get(company.getIn()).isBefore(company.getUpdatedAt()))

						// Run insert-update query
						.flatMapSingle(company ->
						{
							LOG.debug("-- running insert-update on company");

							CompanyEntity entity = new CompanyEntity();
							entity.setIn(company.getIn());
							entity.setName(company.getName());
							entity.setLastModifiedAt(company.getUpdatedAt());

							return companyRepository.insertOnConflictUpdate(conn, entity);
						})

						// Process result
						.doOnNext(resultSet ->
						{
							JsonObject result = resultSet.getRows().get(0);
							if (result.getInteger("code") == 1) {
								LOG.debug("---- successfully inserted #{}", result.getLong("company_id"));
								companyInIdMap.put(result.getString("company_in"), result.getLong("company_id"));
								stats.companyInsertCount++;
							} else if (result.getInteger("code") == 2) {
								LOG.debug("---- successfully updated");
								stats.companyUpdateCount++;
							} else {
								LOG.debug("---- unknown operation {}", result.getInteger("code"));
							}
						})

						// Aggregate to single and return.
						.count().toSingle()
				);
	}

	private Single<Void> persistEmployees(SQLConnection conn, Collection<EmployeeDataObject> values)
	{
		return fetchEmployeeUpdatedMap(conn)

				// Execute in context of previous stream
				.flatMap(updatedMap -> Observable.from(values)

						// Filter out employees that doesn't need to be updated
						.filter(employee ->
						{
							if (!updatedMap.containsKey(employee.getEmail())) {
								return true;
							}

							JsonObject jo = updatedMap.get(employee.getEmail());

							// TODO: ITS IMPORTANT TO REMOVE UPDATED EMPLOYEES FROM THE MAP, SINCE THOSE REMAINING WILL BE REMOVED
							// TODO: THIS IS WHY THIS SHOULD NOT BE IN FILTER FUNCTION - EASY TO OVERLOOK
							updatedMap.remove(employee.getEmail());

							if (jo.getInstant("user_updated_at").isBefore(employee.getUpdatedAt())) {
								return true;
							} else {
								LOG.debug("-- employee already up to date");
								return false;
							}
						})

						// Run insert-update query
						.flatMapSingle(employee ->
						{
							LOG.debug("-- running insert-update on employee");

							EmployeeEntity entity = new EmployeeEntity();
							entity.setEmail(employee.getEmail());
							entity.setCompanyId(companyInIdMap.get(employee.getCompany().getIn()));
							entity.setFirstname(employee.getFirstname());
							entity.setSurname(employee.getSurname());
							entity.setLastModifiedAt(employee.getUpdatedAt());

							return employeeRepository.insertOnConflictUpdate(conn, entity);
						})

						// Process result
						.doOnNext(resultSet ->
						{
							JsonObject result = resultSet.getRows().get(0);
							if (result.getInteger("code") == 1) {
								LOG.debug("---- successfully inserted #{}", result.getLong("employee_id"));
								stats.employeeInsertCount++;
							} else if (result.getInteger("code") == 2) {
								LOG.debug("---- successfully updated");
								stats.employeeUpdateCount++;
							} else {
								LOG.debug("---- unknown operation {}", result.getInteger("code"));
							}
						})

						.count()
						.toSingle()

						// TODO: THIS PROBABLY SHOULD NOT BE CALLED FROM EMPLOYEE?
						.flatMap(resultSet -> clearRemains(conn, updatedMap.values()))
				);
	}

	/**
	 * TODO: companies and employees have slightly different format, better to unify that
	 * @param conn
	 * @return
	 */
	private Single<Map<String, Instant>> fetchCompanyUpdatedMap(SQLConnection conn)
	{
		return companyRepository.findAllUpdatedAt(conn)
				.map(jsonObjects -> jsonObjects.stream()

						// TODO: should not be here or change method name
						.map(entries ->
						{
							LOG.debug("Populating companyInIdMap with existing id: {} -> {}",
									entries.getString("company_in"),
									entries.getLong("company_id"));

							companyInIdMap.put(entries.getString("company_in"), entries.getLong("company_id"));

							return entries;
						})

						.collect(Collectors.toMap(
								jo -> jo.getString("company_in"),
								jo -> jo.getInstant("user_updated_at"))
						));
	}

	/**
	 * TODO: companies and employees have slightly different format, better to unify that
	 * @param conn
	 * @return
	 */
	private Single<Map<String, JsonObject>> fetchEmployeeUpdatedMap(SQLConnection conn)
	{
		return employeeRepository.findAllUpdatedAt(conn)
				.map(jsonObjects -> jsonObjects.stream()
						.collect(Collectors.toMap(
								jo -> jo.getString("email"),
								jo -> jo)
						));
	}

	private Single<Void> clearRemains(SQLConnection conn, Collection<JsonObject> removalList)
	{
		LOG.info("-- running data removal for {} employees", removalList.size());

		if (removalList.isEmpty()) {
			return Single.just(null);
		}

		List<Long> removalIds = removalList.stream()
				.map(jo -> jo.getLong("employee_id"))
				.collect(Collectors.toList());

		return employeeRepository.deleteAll(conn, removalIds)
				.doOnSuccess(resultSet ->
				{
					//JsonObject result = resultSet.getRows().get(0);
					stats.employeeDeleteCount = removalIds.size();
				})
				.flatMap(resultSet -> companyRepository.deleteAllWithoutEmployees(conn))
				.doOnSuccess(resultSet ->
				{
					JsonObject result = resultSet.getRows().get(0);
					stats.companyDeleteCount = result.getInteger("count");
				})
				.map(resultSet -> null);
	}
}

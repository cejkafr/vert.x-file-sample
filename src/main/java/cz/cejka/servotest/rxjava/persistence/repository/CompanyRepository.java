package cz.cejka.servotest.rxjava.persistence.repository;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.rxjava.ext.sql.SQLConnection;
import rx.Single;

import java.util.List;

public interface CompanyRepository
{
	Single<List<JsonObject>> findAllUpdatedAt(SQLConnection conn);
	Single<ResultSet> insertOnConflictUpdate(SQLConnection conn, CompanyEntity entity);
	Single<ResultSet> deleteAllWithoutEmployees(SQLConnection conn);
}

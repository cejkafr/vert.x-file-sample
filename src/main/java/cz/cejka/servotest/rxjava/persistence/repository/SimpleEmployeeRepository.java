package cz.cejka.servotest.rxjava.persistence.repository;

import cz.cejka.servotest.persistence.PersistenceUtils;
import cz.cejka.servotest.persistence.QueryUtils;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.rxjava.ext.sql.SQLConnection;
import rx.Single;

import java.time.Instant;
import java.util.List;
import java.util.Properties;

public class SimpleEmployeeRepository implements EmployeeRepository
{
	private final String queryFindAllUpdatedAt;
	private final String queryInsertOnConflictUpdate;
	private final String queryDeleteAllUnmodifiedSince;
	private final String queryDeleteAllInList;

	public final static String DEF_QUERY_FILE = "db/employee-queries.xml";

	public SimpleEmployeeRepository()
	{
		Properties prop = PersistenceUtils.loadQueries(DEF_QUERY_FILE);
		this.queryFindAllUpdatedAt = prop.getProperty("find-all-updated-at");
		this.queryInsertOnConflictUpdate = prop.getProperty("insert-update");
		this.queryDeleteAllUnmodifiedSince = prop.getProperty("delete-all-unmodified-since");
		this.queryDeleteAllInList = prop.getProperty("delete-all-in-list");
	}

	@Override
	public Single<List<JsonObject>> findAllUpdatedAt(SQLConnection conn)
	{
		return conn.rxQuery(queryFindAllUpdatedAt)
				.map(ResultSet::getRows);
	}

	@Override
	public Single<ResultSet> insertOnConflictUpdate(SQLConnection conn, EmployeeEntity entity)
	{
		Instant instant = Instant.now();
		JsonArray arr = new JsonArray()
				.add(entity.getEmail())
				.add(entity.getCompanyId())
				.add(entity.getFirstname())
				.add(entity.getSurname())
				.add(entity.getLastModifiedAt())
				.add(instant)
				.add(instant)
				.add(entity.getCompanyId())
				.add(entity.getFirstname())
				.add(entity.getSurname())
				.add(entity.getLastModifiedAt())
				.add(instant);

		return conn.rxQueryWithParams(queryInsertOnConflictUpdate, arr);
	}

	@Override
	public Single<ResultSet> deleteAllUnmodifiedSince(SQLConnection conn, Instant updatedAt)
	{
		JsonArray arr = new JsonArray().add(updatedAt);
		return conn.rxQueryWithParams(queryDeleteAllUnmodifiedSince, arr);
	}

	@Override
	public Single<ResultSet> deleteAll(SQLConnection conn, List<Long> ids)
	{
		return conn.rxQuery(QueryUtils.createInQuery(queryDeleteAllInList, 1, ids.toArray(new Long[ids.size()])));
	}
}

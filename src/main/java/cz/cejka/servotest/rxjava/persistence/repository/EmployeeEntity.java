package cz.cejka.servotest.rxjava.persistence.repository;

import java.time.Instant;

public class EmployeeEntity
{
	private long id;
	private long companyId;
	private String email;
	private String firstname;
	private String surname;
	private Instant lastModifiedAt;

	private Instant createdAt;
	private Instant updatedAt;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getCompanyId()
	{
		return companyId;
	}

	public void setCompanyId(long companyId)
	{
		this.companyId = companyId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFirstname()
	{
		return firstname;
	}

	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public Instant getLastModifiedAt()
	{
		return lastModifiedAt;
	}

	public void setLastModifiedAt(Instant lastModifiedAt)
	{
		this.lastModifiedAt = lastModifiedAt;
	}

	public Instant getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(Instant createdAt)
	{
		this.createdAt = createdAt;
	}

	public Instant getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(Instant updatedAt)
	{
		this.updatedAt = updatedAt;
	}
}

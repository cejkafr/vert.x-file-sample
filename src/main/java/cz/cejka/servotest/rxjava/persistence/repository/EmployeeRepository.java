package cz.cejka.servotest.rxjava.persistence.repository;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.rxjava.ext.sql.SQLConnection;
import rx.Single;

import java.time.Instant;
import java.util.List;

public interface EmployeeRepository
{
	Single<List<JsonObject>> findAllUpdatedAt(SQLConnection conn);
	Single<ResultSet> insertOnConflictUpdate(SQLConnection conn, EmployeeEntity entity);
	Single<ResultSet> deleteAllUnmodifiedSince(SQLConnection conn, Instant updatedAt);
	Single<ResultSet> deleteAll(SQLConnection conn, List<Long> ids);
}

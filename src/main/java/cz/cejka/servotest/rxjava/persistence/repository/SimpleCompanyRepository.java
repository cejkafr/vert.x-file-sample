package cz.cejka.servotest.rxjava.persistence.repository;

import cz.cejka.servotest.persistence.PersistenceUtils;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.rxjava.ext.sql.SQLConnection;
import rx.Single;

import java.time.Instant;
import java.util.List;
import java.util.Properties;

public class SimpleCompanyRepository implements CompanyRepository
{
	private final String queryFindAllUpdatedAt;
	private final String queryInsertOnConflictUpdate;
	private final String queryDeleteAllWithoutEmployees;

	public final static String DEF_QUERY_FILE = "db/company-queries.xml";

	public SimpleCompanyRepository()
	{
		Properties prop = PersistenceUtils.loadQueries(DEF_QUERY_FILE);
		this.queryFindAllUpdatedAt = prop.getProperty("find-all-updated-at");
		this.queryInsertOnConflictUpdate = prop.getProperty("insert-update");
		this.queryDeleteAllWithoutEmployees = prop.getProperty("delete-all-wo-employees");
	}

	@Override
	public Single<List<JsonObject>> findAllUpdatedAt(SQLConnection conn)
	{
		return conn.rxQuery(queryFindAllUpdatedAt)
				.map(ResultSet::getRows);
	}

	@Override
	public Single<ResultSet> insertOnConflictUpdate(SQLConnection conn, CompanyEntity entity)
	{
		Instant instant = Instant.now();
		JsonArray params = new JsonArray()
				.add(entity.getIn())
				.add(entity.getName())
				.add(entity.getLastModifiedAt())
				.add(instant)
				.add(instant)
				.add(entity.getName())
				.add(entity.getLastModifiedAt())
				.add(instant);

		return conn.rxQueryWithParams(queryInsertOnConflictUpdate, params);
	}

	@Override
	public Single<ResultSet> deleteAllWithoutEmployees(SQLConnection conn)
	{
		return conn.rxQuery(queryDeleteAllWithoutEmployees);
	}
}

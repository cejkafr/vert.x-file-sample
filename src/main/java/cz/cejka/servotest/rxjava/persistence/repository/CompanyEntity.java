package cz.cejka.servotest.rxjava.persistence.repository;

import java.time.Instant;

public class CompanyEntity
{
	private long id;
	private String in;
	private String name;
	private Instant lastModifiedAt;

	private Instant createdAt;
	private Instant updatedAt;

	public CompanyEntity()
	{
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getIn()
	{
		return in;
	}

	public void setIn(String in)
	{
		this.in = in;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Instant getLastModifiedAt()
	{
		return lastModifiedAt;
	}

	public void setLastModifiedAt(Instant lastModifiedAt)
	{
		this.lastModifiedAt = lastModifiedAt;
	}

	public Instant getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(Instant createdAt)
	{
		this.createdAt = createdAt;
	}

	public Instant getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(Instant updatedAt)
	{
		this.updatedAt = updatedAt;
	}
}

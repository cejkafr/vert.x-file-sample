package cz.cejka.servotest.rxjava.dataprocessor;

import cz.cejka.servotest.rxjava.datareader.DataReader;
import rx.Single;

/**
 * Simple abstraction, we can technically have more processors, with different way of handling data.
 * DataProcessors reads data, separates them into correct classes and creates statistics for entered data.
 *
 * @author fcejka
 */
public interface DataProcessor
{
	static Single<DataProcessorResult> processSimple(DataReader reader)
	{
		return new SimpleDataProcessor().process(reader);
	}

	Single<DataProcessorResult> process(DataReader reader);
}

package cz.cejka.servotest.rxjava.dataprocessor;

import cz.cejka.servotest.rxjava.datareader.DataEntry;
import cz.cejka.servotest.rxjava.datareader.DataReader;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import rx.Single;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple data processor, for small to medium data amount. Loads ALL data into RAM.
 * TODO: possible danger, single processor cannot run multiple processings at one time.
 *
 * @author fcejka
 */
public class SimpleDataProcessor implements DataProcessor
{
	private class Stats
	{
		public int totalEntryCount = 0;
		public int companyCount = 0;
		public int duplicateCompanyCount = 0;
		public int employeeCount = 0;
		public int duplicateEmployeeCount = 0;

		public JsonObject toJson()
		{
			return new JsonObject()
					.put("totalEntryCount", totalEntryCount)
					.put("companyCount", companyCount)
					.put("duplicateCompanyCount", duplicateCompanyCount)
					.put("employeeCount", employeeCount)
					.put("duplicateEmployeeCount", duplicateEmployeeCount);
		}
	}

	private final static Logger LOG = LoggerFactory.getLogger(DataProcessor.class);

	private Map<String, CompanyDataObject> companyDataObjectMap;
	private Map<String, EmployeeDataObject> employeeDataObjectMap;
	private Stats stats;

	public SimpleDataProcessor()
	{
		// constructor is dedicated to configurations
	}

	@Override
	public Single<DataProcessorResult> process(DataReader reader)
	{
		companyDataObjectMap = new HashMap<>();
		employeeDataObjectMap = new HashMap<>();
		stats = new Stats();

		return reader.toObservable()
				.doOnNext(dataEntry ->
				{
					LOG.debug("Processing new entry.");

					// Company must be always first!
					processCompany(dataEntry);
					processEmployee(dataEntry);
				})

				// Error handler
				.doOnError(LOG::error)

				// Aggregate entries and return result.
				.count().toSingle()
				.map(integer ->
				{
					stats.totalEntryCount = integer;
					return new DataProcessorResult(companyDataObjectMap, employeeDataObjectMap, stats.toJson());
				});
	}

	private void processCompany(DataEntry dataEntry)
	{
		// TODO: run exists check before mapping new employee may save some time and memory.
		CompanyDataObject c = mapCompany(dataEntry);
		CompanyDataObject cExist = companyDataObjectMap.getOrDefault(c.getIn(), null);

		// This is where we check, that somebody has posted one company in single file with multiple names.
		if (cExist != null) {

			// Ok so company exists and it has different name.
			if (!c.getName().equals(cExist.getName())) {
				LOG.debug("-- company already exists and it's DUPLICATE ({}, {})", c.getName(), cExist.getName());

				// We always use later modification.
				if (c.getUpdatedAt().isAfter(cExist.getUpdatedAt())) {
					LOG.debug("---- duplicate is newer, updating entry");

					cExist.updateWith(c);
				}

				stats.duplicateCompanyCount++;
			} else {
				LOG.debug("-- company already exists, but it's match");
			}

		// Company doesn't exist yet, lets create it.
		} else {
			LOG.debug("-- company does not exist, creating new entry");

			companyDataObjectMap.put(c.getIn(), c);

			stats.companyCount++;
		}
	}

	/**
	 * Maps data entry to data object.
	 *
	 * TODO: Add validation.
	 *
	 * @param dataEntry
	 * @return
	 */
	private CompanyDataObject mapCompany(DataEntry dataEntry)
	{
		return new CompanyDataObject(
				dataEntry.getCompanyIn(),
				dataEntry.getCompanyName(),
				toInstant(dataEntry.getLastModifiedAt()));
	}

	private void processEmployee(DataEntry dataEntry)
	{
		EmployeeDataObject e = mapEmployee(dataEntry);
		EmployeeDataObject eExist = employeeDataObjectMap.getOrDefault(e.getEmail(), null);

		// Employee was already defined once in our file.
		if (eExist != null) {
			LOG.debug("-- employee is DUPLICATE");

			// We use later modification.
			if (e.getUpdatedAt().isAfter(eExist.getUpdatedAt())) {
				LOG.debug("---- duplicate is newer, replacing entry");

				employeeDataObjectMap.put(e.getEmail(), e);
			}

			stats.duplicateEmployeeCount++;

		// Employee was never defined yet.
		} else {
			LOG.debug("-- employee does not exist, creating new entry");

			employeeDataObjectMap.put(e.getEmail(), e);

			stats.employeeCount++;
		}
	}

	/**
	 * Maps data entry to data object.
	 *
	 * TODO: Add validation.
	 *
	 * @param dataEntry
	 * @return
	 */
	private EmployeeDataObject mapEmployee(DataEntry dataEntry)
	{
		if (!companyDataObjectMap.containsKey(dataEntry.getCompanyIn())) {
			throw new DataProcessorException("Company IN " + dataEntry.getCompanyIn() + " for employee " + dataEntry.getEmployeeEmail() + " not found.");
		}

		return new EmployeeDataObject(
				dataEntry.getEmployeeEmail(),
				companyDataObjectMap.get(dataEntry.getCompanyIn()),
				dataEntry.getEmployeeFirstname(),
				dataEntry.getEmployeeSurname(),
				toInstant(dataEntry.getLastModifiedAt()));
	}

	private Instant toInstant(String updatedAt)
	{
		// Fail handling.
		return Instant.parse(updatedAt);
	}
}

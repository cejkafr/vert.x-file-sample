package cz.cejka.servotest.rxjava.dataprocessor;

import java.time.Instant;
import java.util.Objects;

public class EmployeeDataObject
{
	private final String email;
	private final CompanyDataObject company;
	private final String firstname;
	private final String surname;
	private final Instant updatedAt;

	public EmployeeDataObject(String email, CompanyDataObject company, String firstname, String surname, Instant updatedAt)
	{
		this.email = email;
		this.company = company;
		this.firstname = firstname;
		this.surname = surname;
		this.updatedAt = updatedAt;
	}

	public String getEmail()
	{
		return email;
	}

	public CompanyDataObject getCompany()
	{
		return company;
	}

	public String getFirstname()
	{
		return firstname;
	}

	public String getSurname()
	{
		return surname;
	}

	public Instant getUpdatedAt()
	{
		return updatedAt;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof EmployeeDataObject)) return false;
		EmployeeDataObject that = (EmployeeDataObject) o;
		return Objects.equals(email, that.email) &&
				Objects.equals(company, that.company) &&
				Objects.equals(firstname, that.firstname) &&
				Objects.equals(surname, that.surname) &&
				Objects.equals(updatedAt, that.updatedAt);
	}

	@Override
	public int hashCode()
	{

		return Objects.hash(email, company, firstname, surname, updatedAt);
	}
}

package cz.cejka.servotest.rxjava.dataprocessor;

public class DataProcessorException extends RuntimeException
{
	public DataProcessorException(String message)
	{
		super(message);
	}

	public DataProcessorException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

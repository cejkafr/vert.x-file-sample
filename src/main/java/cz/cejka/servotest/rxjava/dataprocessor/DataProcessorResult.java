package cz.cejka.servotest.rxjava.dataprocessor;

import io.vertx.core.json.JsonObject;

import java.util.Map;

public class DataProcessorResult
{
	public final Map<String, CompanyDataObject> companyList;
	public final Map<String, EmployeeDataObject> employeeList;
	public final JsonObject stats;

	public DataProcessorResult(Map<String, CompanyDataObject> companyList, Map<String, EmployeeDataObject> employeeList, JsonObject stats)
	{
		this.companyList = companyList;
		this.employeeList = employeeList;
		this.stats = stats;
	}
}

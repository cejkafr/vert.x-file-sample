package cz.cejka.servotest.rxjava.dataprocessor;

import java.time.Instant;

public class CompanyDataObject
{
	private final String in;
	private String name;
	private Instant updatedAt;

	public CompanyDataObject(String in, String name, Instant updatedAt)
	{
		this.in = in;
		this.name = name;
		this.updatedAt = updatedAt;
	}

	public String getIn()
	{
		return in;
	}

	public String getName()
	{
		return name;
	}

	public Instant getUpdatedAt()
	{
		return updatedAt;
	}

	public void updateWith(CompanyDataObject c)
	{
		this.name = c.getName();
		this.updatedAt = c.updatedAt;
	}
}

package cz.cejka.servotest.persistence;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PersistenceUtils
{
	private final static Logger LOG = LoggerFactory.getLogger(PersistenceUtils.class);

	public static Properties loadQueries(String path)
	{
		final Properties queries = new Properties();
		try (InputStream is = PersistenceUtils.class.getClassLoader().getResourceAsStream(path)) {
			if (is == null)
				throw new RuntimeException("Query property file " + path + " not found.");
			queries.loadFromXML(is);
		} catch (IOException ex) {
			throw new RuntimeException("Unable to read db query file.");
		}

		LOG.debug("Loaded database queries : {}.", queries.keySet());

		return queries;
	}

}

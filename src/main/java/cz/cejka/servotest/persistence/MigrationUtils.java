package cz.cejka.servotest.persistence;

import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfoService;

/**
 * Provides simplification and abstraction for database migrations.
 *
 * @author fcejka
 */
public class MigrationUtils
{
	private final static Logger LOG = LoggerFactory.getLogger(MigrationUtils.class);

	public final static String KEY_INITIALIZE_EMPTY = "initializeEmpty";
	public final static String KEY_MIGRATION_STRATEGY = "migrationStrategy";
	public final static boolean DEF_INITIALIZE_EMPTY = true;
	public final static String DEF_MIGRATION_STRATEGY = "update";

	/**
	 * Executes database migrations on enhanced vert.x datasource configuration.
	 * Use initializeEmpty and migrationStrategy to set expected behaviour.
	 *
	 * @param config Enhanced vert.x datasource configuration.
	 * @return
	 */
	public static void migrate(JsonObject config)
	{
		LOG.info("Flyway: running database migrations.");

		final Flyway flyway = new Flyway();
		flyway.setValidateOnMigrate(true);
		flyway.setDataSource(
				/*buildConnUrl(config),*/
				config.getString("url"),
				config.getString("user"),
				config.getString("password"));

		final MigrationInfoService info = flyway.info();
		final boolean empty = info.applied().length == 0;
		final boolean initializeEmpty = config.getBoolean(KEY_INITIALIZE_EMPTY, DEF_INITIALIZE_EMPTY);
		final String strategy = config.getString(KEY_MIGRATION_STRATEGY, DEF_MIGRATION_STRATEGY);

		if (initializeEmpty && empty) {
			LOG.info("Flyway: Empty schema; applying {} migrations.",
					info.pending().length);
			flyway.migrate();
		} else if (!empty) {
			switch (strategy) {
				case "update":
					LOG.info("Flyway: update strategy; applying {} of {} migrations.",
							info.pending().length, info.all().length);
					flyway.migrate();
					break;
				case "reinitialize":
					LOG.info("Flyway: reinitialize strategy; removing {} migrations, applying {} migrations.",
							info.applied().length, info.all().length);
					flyway.clean();
					flyway.migrate();
					break;
				case "validate":
					LOG.info("Flyway: validate strategy;.");
					flyway.validate();
				default:
					LOG.warn("Flyway: migration strategy '" + strategy + "' is not known - update, reinitialize, validate.");
			}
		} else {
			LOG.info("Flyway: Empty schema; doing nothing.");
		}
	}

	/**
	 * Very quick and basic, expects only postgresql.
	 * TODO: generalize for all JDBC databases
	 *
	 * @param config Datasource configuration.
	 * @return JDBC connnection URL.
	 */
	private static String buildConnUrl(JsonObject config)
	{
		return new StringBuilder("jdbc:postgresql://")
				.append(config.getString("host", "localhost"))
				.append(":")
				.append(config.getInteger("port", 5432))
				.append("/")
				.append(config.getString("database", "testdb"))
				.toString();
	}
}

package cz.cejka.servotest.persistence;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * @author fcejka
 */
public class QueryUtils
{
	final static Logger LOG = LoggerFactory.getLogger(QueryUtils.class);

	public static String createInQuery(String inQuery, int argNo, Long[] values)
	{
		StringBuilder sb;

		sb = new StringBuilder();
		for (long value : values){
			sb.append(value).append(",");
		}

		String valuesStr = sb.substring(0, sb.length() - 1);
		int argPos = findArgPositionInQuery(inQuery, argNo);

		sb = new StringBuilder();
		String outQuery = sb.append(inQuery.substring(0, argPos))
				.append(valuesStr)
				.append(inQuery.substring(argPos + 1))
				.toString();

		return outQuery;
	}

	public static int findArgPositionInQuery(String inQuery, int argNo)
	{
		int startPos = 0;

		for (int i = 0; i < argNo; i++) {
			startPos = inQuery.indexOf("?", startPos > 0 ? startPos + 1 : startPos);
		}

		return startPos;
	}
}

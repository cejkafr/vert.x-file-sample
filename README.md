Slovo úvodem
-----------

Pro řešení jsem zvolil Vert.x, primárně ze tří důvodů:
1. poslední 1 rok sem dělal pouze v něm => nejrychlejší řešení
2. chtěl jsem předvést ne moc známou technologii, kterou sem se v minulém roce naučil
3. dovolil sem si předpokládat že aplikace bude součást nějakého většího řešení, clusteru, apod.

Rozhodně to není optimální řešení v případě, že by šlo jen o minimalistický jednorázový nástroj. Reaktivita také nepřináší moc benefitů.
Mít více času tak obecná logika celého řešení může být abstraktní a spustitelná i bez vertx, současné řešení je hodně provázané. 


Vyžaduje (či testováno na)
-----------
- Maven 3>
- JDK8
- PostgresSQL 9.5> - používá ON CONFLICT


Kompilace
-----------
Spustit _mvn package_ výsledné fat jar v /target
 

Konfigurace
-----------
Prostřednictvím .json souboru, ukázkový je k nalezení v conf/default.json.
Dovolím si předpkládat že parametry jsou dost srozumitelné a nebudu je rozepisovat.

Migraci databáze je možné provést nastavením datasource v konfiguraci.
- initializeEmpty [boolean] (true) - namigruje prázdné schéma
- migrationStrategy [string] (reinitialize) - reinitialize - vždy vyčistí databázi a provede novou migraci; validate - pouze zvaliduje schéma; update - pouze aktualizuj schéma 


Jak spustit
-----------

JAR

Spustit _java -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory -jar target/servotest-0.0.01-SNAPSHOT.jar run cz.cejka.servotest.rxjava.verticle.DataImportVerticle -conf conf/default.json_.

-----------

IDE - Idea (jiné nemám)

1. Main class je _io.vertx.core.Launcher_ (pozor, některé IDE, např netbeans neumí main class z jinych package, je třeba vytvořit lokální třídu, která jen dědí tuto)
2. Program arguments _run cz.cejka.servotest.rxjava.verticle.DataImportVerticle -conf conf/default.json_
3. VM options _-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory_



Stručný popis implementace
-----------


Entry point
-----------
Vstupním bodem aplikace je DataImportVerticle, provede spojení s databází, kontrolu migrace, poté se nastaví časovač, který jednou v zadaný čas spustí proces vyhledání souborů v adresáři a jejich zpracování. 


DataReader
-----------
Cesta souboru je předána DataReaderFactory, ověřením přípony se zjistí, zda je nadefinovám použitelný reader a pokud ano, je vrácen.
Aktuální imlementace obsahuje pouze JsonDataReader, který čte data ze souboru streamovaně.


DataProcessor
-----------
Čte jednotlivé záznamy z DataReaderu a vytváří si lokální in-memory databázi dat, ověřuje duplicitní záznamy:

- firma se stejným IČ uvedena 2x s různým názvem, použije se záznam s pozdějším datem modifikace
- zaměstnanec s emailem, jež je uveden vícekrát, použije se záznam s pozdějším datem modifikace

Není optimalizován pro zpracování BigData apod., postupně načte celý soubor do paměti, tedy řešení předpokládá že toto není problém.


DataPersister  
-----------
Výsledek DataProcessoru je předán DataPersisteru, nejprve jsou zpracovány firmy poté zaměstnanci, celé jako jedna transakce.

- pro každý záznam INSERT ON CONFLICT UPDATE, nejsou zde batchované dotazy
- dále vymaže všechny zaměstnance, kteří nebyli modifikování - tedy nebyli v novém datasetu
- dále vymaže všechny firmy, které nemají zaměstnance - opět nebyli v novém datasetu

Řešení opět předpokládá že zátěž databáze v podobě 2 query per záznam v souboru není problém.


Co zde není?  
-----------
Validace, unit testy, lepší error handling - spousta věcí popsána v TODO, určitě by si to zasloužilo delší testování, ale trochu sem se rozvášnil při návrhu a vývoji, že mi na to nezbyl čas.


Struktura JSON vstupu 
-----------
[
	{
		"companyIn": "az1234",
		"companyName": "AZ1234",
		"employeeEmail": "email0@email",
		"employeeFirstname": "Firstname0",
		"employeeSurname": "Surname0",
		"lastModifiedAt": "2014-01-01T00:00:00.000Z"
	}
]
